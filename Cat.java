package cus1156.catlab2;

import java.util.Scanner;
import java.util.ArrayList;


public class Cat
	{
      private String name;
      private String colour;
		
      public Cat() { 
    	  
      } 
      
      public Cat(String name, String colour)
    	  {
    		  this.name = name;
    		  this.colour = colour;
    	  }
      
		private void meow()
			{
				System.out.println("Meow!");
			}
		
		public void manyMeows(int howMany)
			{
			
				System.out.println(name + " says ");
				int count=0;
				while (count++ < howMany)
					meow();
			}
		
		public void printMyName()
			{
				System.out.println("my name is " + name);
			}

		public String getName()
			{
				return name;
			}

		public void setName(String name)
			{
				this.name = name;
			}

		public String getColour()
			{
				return colour;
			}

		public void setColour(String colour)
			{
				this.colour = colour;
			}
	}
