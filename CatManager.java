package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private ArrayList<Cat>  myCats;

	public CatManager()
	{
		myCats = new ArrayList<>();
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
		cat = new Cat("Yummi", "Persian");
		myCats.add(cat);
	}

	/** add a cat to the group
	 * 
	 * @param name
	 * @return
	 */
	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}


	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
	 * 
	 * @param name - the name of the Cat that we are searching for
	 * @return the Cat object or null if not found
	 */
	public Cat findThisCat(String name)
	{
		for (Cat c:myCats) {
			if(c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

	// returns the number of cats of this colour
	/**
	 * returns the number of cats of this colour
	 * @param colour - the colour we are counting
	 * @return - the number of cats
	 */
	public int countColours(String colour)
	{           
		int count = 0;
		for (Cat c : myCats) {
			if(c.getColour().equals(colour)) {
				count++;
			}
		}
		return count;
	}
}
