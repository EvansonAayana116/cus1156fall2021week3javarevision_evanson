package cus1156.catlab2;

import java.util.Scanner;


import java.util.Scanner;


public class Lab2Driver
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		CatManager catMgr = new CatManager();

		int numTabby = catMgr.countColours("tabby");
		int numSpotted = catMgr.countColours("spotted");
		System.out.println("There are " + numTabby + " tabby cats");
		System.out.println("There are " + numSpotted + " spotted cats");

		System.out.println("Enter the name of the cat you would like to find");
		String name =  input.next();
		Cat foundCat = catMgr.findThisCat(name);
		if (foundCat == null)
			System.out.println("did not find this cat");
		else
			foundCat.manyMeows(3);
		
		input.close();
	}

}
